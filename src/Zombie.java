public class Zombie extends Entity{
    private boolean dead = false;

    protected Zombie(String filename, double x, double y) {
        super(filename, x, y);
    }

    protected boolean isDead() {
        return dead;
    }

    protected void killed() {
        dead = true;
    }
}