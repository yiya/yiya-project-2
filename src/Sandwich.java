public class Sandwich extends Entity{
    private boolean eaten = false;

    protected Sandwich(String filename, double x, double y) {
        super(filename, x, y);
    }

    protected void eaten() {
        this.eaten = true;
    }

    protected boolean isEaten() {
        return eaten;
    }
}