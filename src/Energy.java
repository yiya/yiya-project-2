public class Energy {
    private int value;

    protected Energy(int energy) {
        this.value = energy;
    }

    protected int getEnergy() {
        return this.value;
    }

    protected void add(int amount) {
        this.value += amount;
    }
}
