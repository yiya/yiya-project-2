import java.util.ArrayList;

public class Bullet extends Entity implements Movable {
    private double xDirection, yDirection;

    protected Bullet(String filename, double x, double y, double xDirection, double yDirection) {
        super(filename, x, y);
        this.xDirection = xDirection;
        this.yDirection = yDirection;
    }

    public double getxDirection() {
        return xDirection;
    }

    public double getyDirection() {
        return yDirection;
    }

    protected void kill(ArrayList<Zombie> zombies, int index) {
        zombies.get(index).killed();
    }

    @Override
    public void move(double xStep, double yStep) {
        this.setX(this.getX() + xStep);
        this.setY(this.getY() + yStep);
    }

}
