public interface Movable {
    public void move(double xStep, double yStep);
}