public class Treasure extends Entity {
    private boolean found = false;

    protected Treasure(String filename, double x, double y) {
        super(filename, x, y);
    }

    protected void found() {
        found = true;
    }

    protected boolean isFound() {
        return found;
    }
}