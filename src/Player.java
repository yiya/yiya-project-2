import java.util.ArrayList;

public class Player extends Entity implements Movable{
    private Energy energy;

    protected Player(String filename, double x, double y, int energy) {
        super(filename, x, y);
        this.energy = new Energy(energy);
    }

    protected int getEnergy() {
        return energy.getEnergy();
    }

    protected void updateEnergy(int amount) {
        this.energy.add(amount);
    }

    protected void eat(ArrayList<Sandwich> sandwiches, int index) {
        this.updateEnergy(+5);
        sandwiches.get(index).eaten();
    }

    @Override
    public void move(double xStep, double yStep) {
        this.setX(this.getX() + xStep);
        this.setY(this.getY() + yStep);
    }
}
