import bagel.*;
import bagel.util.Colour;
import bagel.util.Vector2;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


/**
 * An example Bagel game.
 */
public class ShadowTreasure extends AbstractGame {
    private String filename = "res/IO/environment.csv";
    private final Image background = new Image("res/images/background.png");
    private Player player;
    private Zombie zombie;
    private Sandwich sandwich;
    private final Font energyLevel = new Font("res/font/DejaVuSans-Bold.ttf", 20);
    private final DrawOptions black = new DrawOptions().setBlendColour(Colour.BLACK);
    private int frame = 0;

    // for rounding double number; use this to print the location of the player
    private static DecimalFormat df = new DecimalFormat("0.00");

    public static void printInfo(double x, double y, int e) {
        System.out.println(df.format(x) + "," + df.format(y) + "," + e);
    }

    public ShadowTreasure() throws IOException {
        this.loadEnvironment(filename);
        // Add code to initialize other attributes as needed
    }

    /**
     * Load from input file
     */
    private void loadEnvironment(String filename) throws FileNotFoundException {
        // Code here to read from the file and set up the environment
        Scanner scan = new Scanner(new File(filename));
        String[] row;
        while (scan.hasNext()) {
            row = scan.next().split(",",4);
            switch (row[0]) {
                case "Player" :
                    this.player = new Player("res/images/player.png",
                                            Double.parseDouble(row[1]),
                                            Double.parseDouble(row[2]),
                                            Integer.parseInt(row[3]));
                    break;
                case "Zombie" :
                    this.zombie = new Zombie("res/images/zombie.png",
                                             Double.parseDouble(row[1]),
                                             Double.parseDouble(row[2]));
                    break;
                case "Sandwich" :
                    this.sandwich = new Sandwich("res/images/sandwich.png",
                                                 Double.parseDouble(row[1]),
                                                 Double.parseDouble(row[2]));
                    break;
            }
        }
        printInfo(player.getX(), player.getY(), player.getEnergy());
    }

    /**
     * Performs a state update.
     */
    @Override
    public void update(Input input) {
        // Logic to update the game, as per specification must go here
        if (input.wasPressed(Keys.ESCAPE)) {
            Window.close();

        } else if (frame<10) {
            background.draw(Window.getWidth() / 2.0, Window.getHeight() / 2.0);
            player.draw(player.getX(), player.getY());
            zombie.draw(zombie.getX(), zombie.getY());
            if (!sandwich.isEaten()) {
                sandwich.draw(sandwich.getX(), sandwich.getY());
            }
            energyLevel.drawString("energy: " + Integer.toString(player.getEnergy()),
                    20, 760, black);
            frame++;
            return;
        }

        double speed = 10;
        double playerToZombie = player.getPosition().distanceTo(zombie.getPosition());
        double playerToSandwich = player.getPosition().distanceTo(sandwich.getPosition());
        double distDiff = playerToZombie - playerToSandwich;
        int energy = player.getEnergy();
        Vector2 direction;

        if (sandwich.isEaten() || (distDiff <= 0 && energy >= 3)) {
//        if (sandwich.isEaten() || energy >= 3) {
            // player move to zombie
            direction = zombie.getPosition().asVector().sub(player.getPosition().asVector());
            direction = direction.normalised();
            player.setX(player.getX() + speed * direction.x);
            player.setY(player.getY() + speed * direction.y);
            playerToZombie = player.getPosition().distanceTo(zombie.getPosition());

            if (playerToZombie <= 50) {
                player.updateEnergy(-3);
                Window.close();          // terminate game if player meets zombie
            }
//            if (playerToSandwich <= 50) {
//                player.eat(sandwich);
//            }

        } else if (distDiff > 0 || player.getEnergy() < 3) {
//        } else if (player.getEnergy() < 3) {
            // player move to sandwich
            direction = sandwich.getPosition().asVector().sub(player.getPosition().asVector());
            direction = direction.normalised();
            player.setX(player.getX() + speed * direction.x);
            player.setY(player.getY() + speed * direction.y);
            playerToSandwich = player.getPosition().distanceTo(sandwich.getPosition());

//            if (playerToZombie <= 50) {
//                player.updateEnergy(-3);
//                Window.close();          // terminate game if player meets zombie
            if (playerToSandwich <= 50) {
                player.eat(sandwich);
            }
        }


        background.draw(Window.getWidth() / 2.0, Window.getHeight() / 2.0);
        player.draw(player.getX(), player.getY());
        zombie.draw(zombie.getX(), zombie.getY());
        if (!sandwich.isEaten()) {
            sandwich.draw(sandwich.getX(), sandwich.getY());
        }
        energyLevel.drawString("energy: " + Integer.toString(player.getEnergy()),
                20, 760, black);

        frame = 0;
        printInfo(player.getX(), player.getY(), player.getEnergy());
    }


    /**
     * The entry point for the program.
     */
    public static void main(String[] args) throws IOException {
        ShadowTreasure game = new ShadowTreasure();
        game.run();
    }
}
